#include <stdio.h>

int main(){

    int i = 0, eNumeroPar;
    
    printf("Programa que contiene los n%cmeros pares hasta el 100.\n", 163);
    
    while(eNumeroPar < 100){
            
            eNumeroPar = 2 * i;
            i++;
            printf("%d\n", eNumeroPar);
                    
    }    
    
    getchar();
    return 0;
    
}
