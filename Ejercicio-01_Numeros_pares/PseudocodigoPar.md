/*

    Algoritmo: Ejercicio-01_Numeros_pares
    Autor: Héctor Hernández Sánchez
    Fecha: 16/10/2021
    Objetivo: Elabora un programa que imprima los números pares del 0 al 100.

*/

Inicio
    
    eNumeroPar entero
    i = 0  entero

    Escribri "Programa que contiene los números pares hasta el 100." 
    
    mientras (eNumeroPar < 100) entonces
    
        eNumeroPar = 2 * i
        i++
        Escribir eNumeroPar
    
    fin mientras

fin